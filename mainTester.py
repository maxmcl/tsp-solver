"""
Testing main.py scripts
"""

import os
from glob import glob
import subprocess

# Finding tsp instances to test in subdirectories
tspInstances = [y for x in os.walk('.') for y in glob(os.path.join(x[0], '*.tsp'))]

# Calling the local main.py solver using every argument

for aTsp in tspInstances:
    print repr(aTsp), '\n'
    p = subprocess.Popen("python ./main.py -s " + str(aTsp) + " -ts RSL -m prim", shell=True)
    p.communicate()
    # raw_input("Press the <ENTER> key to continue...")

