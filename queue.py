from operator import methodcaller


class Queue(object):
    "Une implementation de la structure de donnees << file >>."

    def __init__(self):
        self.items = []
        self.counter = 0

    def enqueue(self, item):
        "Ajoute `item` a la fin de la file."
        self.items.append(item)
        self.counter += 1

    def dequeue(self):
        "Retire l'objet du debut de la file."
        self.counter -= 1
        return self.items.pop(0)

    def is_empty(self):
        "Verifie si la file est vide."
        return self.counter == 0

    def get_counter(self):
        return self.counter

    def __contains__(self, item):
        return item in self.items

    def __repr__(self):
        return repr(self.items)


class PriorityQueue(Queue):

    def dequeue(self):
        "Retire l'objet ayant la plus haute priorite."
        highest = self.items[0]
        for item in self.items[1:]:
            if item > highest:
                highest = item
        idx = self.items.index(highest)
        self.counter -= 1
        return self.items.pop(idx)

    def enqueue(self, item):
        "Ajoute l'objet selon sa priorite"
        if self.items:
            i = 0
            while (i < len(self.items)) and (self.items[i].get_priority() < item.get_priority()):
                i += 1
            self.items.insert(i, item)
        else:
            self.items.append(item)
        self.counter += 1
        return

    def update(self):
        self.items = sorted(self.items, key=methodcaller('get_priority'))

    def __repr__(self):
        for index, item in enumerate(self.items):
            print 'Item: ' + str(index) + ', Priorite: ' + str(item.get_priority()) + '\n'
        return ''

# Les items de priorite nous permettent d'utiliser min() et max().

if __name__ == "__main__":

    from queue import PriorityQueue
    from node import Node

    p = PriorityQueue()
    
    n1 = Node()
    n1.key = 5
    n2 = Node()
    n2.key = 1
    n3 = Node()
    n3.key = -10
    n4 = Node()
    n4.key = 100

    p.enqueue(n1)
    p.enqueue(n2)
    p.enqueue(n3)
    p.enqueue(n4)

    print p

    n3.set_priority(7)

    p.update()

    print p
