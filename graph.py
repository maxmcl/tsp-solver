class Graph(object):
    """
    Une classe generique pour representer un graphe comme un ensemble de noeuds.
    """

    def __init__(self, name='Sans nom'):
        """
        Constructeur
        :param name: nom du graph (string)
        """
        self.__name = name
        self.__nodes = []  # Attribut prive.
        self.__node_counter = 0
        self.__edges = []
        self.__edge_counter = 0
        # Matrice d'adjacence sous forme de double dictionnaire
        self.__adj = {}

    def add_node(self, node):
        """
        Ajout d'un noeud au graphe
        :param node: objet Node a ajouter
        :return:
        """
        self.__nodes.append(node)
        self.__node_counter += 1

    def add_edge(self, edge):
        """
        Ajout d'une arete au graphe
        :param edge: objet Edge a ajouter
        :return:
        """
        self.__edges.append(edge)
        self.__edge_counter += 1

        # On recupere les noeuds de l'arete
        node1, node2 = edge.get_nodes()

        # On les ajoute a la matrice d'adjacence, le graphe est non oriente
        try:
            self.__adj[node1][node2] = edge
        except KeyError:
            self.__adj[node1] = {node2: edge}
        try:
            self.__adj[node2][node1] = edge
        except KeyError:
            self.__adj[node2] = {node1: edge}

    def get_name(self):
        """
        Retourne le nom du graphe
        :return: name
        """
        return self.__name

    def get_nodes(self):
        """
        Retourne la liste de noeuds du graphe
        :return: liste des noeuds
        """
        return self.__nodes

    def get_edges(self):
        """
        Retourne la liste des aretes du graphe
        :return: liste des aretes
        """
        return self.__edges

    def get_nb_nodes(self):
        """
        Retourne le nombre de noeuds dans le graphe
        :return: nombre de noeuds
        """
        return self.__node_counter

    def get_nb_edges(self):
        """
        Retourne le nombre d'aretes dans le graphe
        :return: nombre d'aretes
        """
        return self.__edge_counter

    def get_graph_weight(self):
        """
        Retourne le poids total du graphe
        :return: somme du poids de toutes les aretes
        """
        weight = 0
        if self.get_nb_edges() != 0:
            # On parcourt les poids et on les somme
            for edge in self.edges:
                weight += edge.get_weight()
        return weight

    def get_adj(self):
        """
        Retourne la matrice d'adjacence
        :return:
        """
        return self.__adj

    def get_edge_between_nodes(self, node1, node2):
        """
        Retourne l'arete entre 2 noeuds selon la matrice d'adjacence
        :param node1:
        :param node2:
        :return: arete entre node1 et node2
        """
        return self.adj[node1][node2]

    def get_neighbor_nodes(self, node):
        """
        Retourne les noeuds voisins a un noeud selon la matrice d'adjacence
        :param node:
        :return: liste de noeuds
        """
        return self.adj[node].keys()

    def get_neighbor_edges(self, node):
        """
        Retourn les aretes connectees a un noeud selon la matrice d'adjacence
        :param node:
        :return: liste d'aretes
        """
        return self.adj[node].values()

    def __repr__(self):
        """
        Surcharge de la fonction print d'un objet graphe
        :return: none
        """
        name = self.get_name()
        nb_nodes = self.get_nb_nodes()
        s = 'Graphe %s comprenant %d noeuds' % (name, nb_nodes)
        for node in self.get_nodes():
            s += '\n  ' + repr(node)

        nb_edges = self.get_nb_edges()
        s += '\nGraphe %s comprenant %d aretes' % (name, nb_edges)
        for edge in self.get_edges():
            s += '\n  ' + repr(edge)
        return s

    def plot_graph(self, s='no_title', check_save=False):
        """
        Methode permettant d'afficher un graphe
        :param s: titre de la figure
        :param check_save: booleen pour activer la sauvegarde au format pdf
        :return: none
        """
        import matplotlib.pyplot as plt
        from matplotlib.collections import LineCollection
        from numpy import asarray

        fig = plt.figure()
        ax = fig.add_subplot(111)

        x = []
        y = []
        edge_pos = []
        names = []

        # On recupere les positions des noeuds
        for node in self.nodes:
            temp_x, temp_y = node.get_data()
            x.append(temp_x)
            y.append(temp_y)
            names.append(node.get_name())

        # On recupere les positions des aretes
        for edge in self.edges:
            node1, node2 = edge.get_nodes()
            temp_x, temp_y = node1.get_data()
            temp_x2, temp_y2 = node2.get_data()
            edge_pos.append([[temp_x, temp_y], [temp_x2, temp_y2]])

        edge_pos = asarray(edge_pos)
        edge_collection = LineCollection(edge_pos, linewidth=1.5, antialiased=True,
                                         colors=(.8, .8, .8), alpha=.75, zorder=0)
        ax.add_collection(edge_collection)
        # On trace
        ax.scatter(x, y, s=35, c='r', antialiased=True, alpha=.75, zorder=1)
        # On ajoute la numerotation des noeuds
        for i, txt in enumerate(names):
            ax.annotate(txt, (x[i], y[i]))
        plt.title(s)
        if check_save is True:
            plt.savefig('Figures/' + s + '.pdf')
        # plt.axis('equal')
        # plt.show()

        return

    # Proprietes
    edges = property(get_edges)
    nodes = property(get_nodes)
    adj = property(get_adj)

    """
                                        --- Main methods ---
    """

    def graph_from_stsp(self, nodes, edges):
        """
        Instancie un graph depuis une liste de noeuds et d'aretes provenant de read_stsp SEULEMENT
        :param nodes: liste de noeuds
        :param edges: liste d'aretes
        :return: none
        """
        from node import Node
        from edge import Edge

        # On doit maintenant convertir les noeuds et les aretes en objets des classes Node et Edge
        if nodes:
            # On cree et ajoute les noeuds
            for node, val in zip(nodes, nodes.values()):
                self.add_node(Node(data=[val[0], val[1]], name=str(node)))
            # On cree et ajoute les aretes
            for edge in edges:
                self.add_edge(Edge(self.nodes[edge[0]], self.nodes[edge[1]], int(edge[2])))
        else:
            import random as rd
            # On doit construire la liste de noeuds...
            nodes = set()
            for edge in edges:
                nodes.add(edge[0])
                nodes.add(edge[1])
            n_nodes = len(nodes)
            row_counter = 0
            column_counter = 0
            # On cree et ajoute les noeuds
            for node in nodes:
                # On fait une sorte de geometrie plus ou moins rectangulaire (purement pour affichage)
                column_counter += 1
                if column_counter > n_nodes / 4:
                    column_counter = 0
                    row_counter += 1
                # Data bidon
                self.add_node(Node(data=[row_counter + rd.random() * 2, column_counter + rd.random() * 2],
                                   name=str(node)))
            # On ajoute les aretes en consequence
            for edge in edges:
                self.add_edge(Edge(self.nodes[edge[0]], self.nodes[edge[1]], int(edge[2])))

    def graph_from_stsp_weights(self, nodes, edges):
        """
        Instancie un graph depuis une liste de noeuds et d'aretes provenant de read_stsp SEULEMENT
        :param nodes: liste de noeuds
        :param edges: liste d'aretes
        :return: none
        """
        from node import Node
        from edge import Edge

        # On doit maintenant convertir les noeuds et les aretes en objets des classes Node et Edge
        # On cree et ajoute les noeuds
        for node in nodes.keys():
            self.add_node(Node(data=node, name=str(node)))
        # On cree et ajoute les aretes
        for edge in edges:
            self.add_edge(Edge(self.nodes[edge[0]], self.nodes[edge[1]], int(edge[2])))


if __name__ == '__main__':

    from node import Node
    from edge import Edge
    import matplotlib.pyplot as plt

    G = Graph(name='Graphe du cours')
    let = ['a', 'b', 'c', 'd', 'e']
    dat = [[0, 0], [5, 5], [0, 5], [5, 0], [3, 3]]
    # let = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
    # dat = [[0, 1], [1, 2], [4, 2], [6, 2], [7, 1], [6, 0], [4, 0], [1, 0], [2, 1]]
    for temp_name, temp_dat in zip(let, dat):
        G.add_node(Node(name=temp_name, data=temp_dat))

    nodes = G.get_nodes()

    for i in range(0, len(let)):
        for j in range(i + 1, len(let)):
            temp_dat1 = nodes[i].get_data()
            temp_dat2 = nodes[j].get_data()
            dist = ((temp_dat1[0] - temp_dat2[0]) ** 2 + (temp_dat1[1] - temp_dat2[1]) ** 2) ** 0.5
            G.add_edge(Edge(nodes[i], nodes[j], weight=dist))

    G.plot_graph()
    plt.show()
