class PriorityItem(object):
    "Un item generique possedant un attribut de priorite."

    def __init__(self, priority=0):
        self.priority = max(0, priority)

    def get_priority(self):
        "Donne la priorite de l'item."
        return self.priority

    def set_priority(self, priority):
        "Affecte une priorite a l'item."
        self.priority = max(0, priority)

    def __lt__(self, other):
        return self.get_priority() < other.get_priority()

    def __le__(self, other):
        return self.get_priority() <= other.get_priority()

    def __eq__(self, other):
        return self.get_priority() == other.get_priority()

    def __ne__(self, other):
        return self.get_priority() != other.get_priority()

    def __gt__(self, other):
        return self.get_priority() > other.get_priority()

    def __ge__(self, other):
        return self.get_priority() >= other.get_priority()

    #def __cmp__(self, other):
    #    mine = self.get_priority()
    #    theirs = other.get_priority()
    #    if mine < theirs:
    #        return -1         # Signifie que self < other.
    #    if mine > theirs:
    #        return 1          # Signifie que self > other.
    #    return 0              # Signifie que self = other.

if __name__ == '__main__':

    item1 = PriorityItem(priority=1)
    item2 = PriorityItem(priority=3)

    print item1 <  item2
    print item1 <= item2
    print item1 == item2
    print item1 != item2
    print item1 >  item2
    print item1 >= item2
