
from graph import Graph
from disjoint_set import DisjointSet
from operator import methodcaller
import heapq
from priority_dict import priority_dict


class MstBuilder(Graph):
    """
    Sous classe de Graph permettant de construire un minimum spanning tree
    Implemente les algorithmes de Kruskal et Prim
    """

    def __init__(self, **kwargs):
        # On fait appel au constructeur de Graph
        super(MstBuilder, self).__init__(**kwargs)

        # On ajoute un dictionnaire de degres des noeuds pour la methode de HK
        self.__node_degree = {}

    def kruskal(self, graph, source=None):
        """
        Construction d'un minimum spanning tree selon l'algorithme de Kruskal
        :param graph: graphe a partir duquel construire le MST
        :param source: pas utilise, mais on laisse le meme nombre d'arguments pour toutes les methodes
        :return: none
        """
        # On cree la foret d'ensemble disjoints
        tree = DisjointSet()
        for node in graph.nodes:
            # On initialise tous les noeuds
            tree.make_set(node)
            # Le MST contiendra obligatoirement tous les noeuds
            self.add_node(node)

        # On trie la liste d'arete selon le poids
        edges = sorted(graph.edges, key=methodcaller('get_weight'))
        for edge in edges:
            # On recupere les noeuds de l'arete en cours
            node1, node2 = edge.get_nodes()
            # Si les deux noeuds sont dans des ensembles disjoints differents
            # MODIF: on rajoute le check HK_node pour rendre compatible avec l'algo de HK
            if (tree.find_set(node1) != tree.find_set(node2)) and (not node1.hk_node) and (not node2.hk_node):
                # On les unit
                tree.union(node1, node2)
                # On ajoute l'arete au graph
                self.add_edge(edge)
                # On n'a besoin que d'ajouter # nodes - 1 aretes pour faire un arbre
                if self.get_nb_edges() >= self.get_nb_nodes() - 1:
                    break
        return

    def prim(self, graph, source):
        """
        Construction d'un minimum spanning tree selon l'algorithme de Prim
        :param graph: graphe a partir duquel construire le MST
        :param source: noeud de depart de l'algorithme
        :return: none
        """
        # On initialise un dictionnaire avec priorite
        Q = priority_dict()
        # On initialise les noeuds
        for node in graph.nodes:
            node.key = float("inf")
            node.parent = None
            # On remplit le dictionnaire avec chaque noeud
            Q[node] = node.key
        # On initialise la source et on met a jour le dictionnaire
        source.key = 0
        Q[source] = source.key

        # On itere tant qu'il y a des noeuds dans Q
        while Q:
            node = Q.pop_smallest()
            # On recupere les voisins du noeud courant
            for neighbor in graph.get_neighbor_nodes(node):
                # Si un des noeuds voisins n'a pas encore ete visite
                # MODIF: on ajoute le check pour un noeud deconnecte pour compatibilite avec HK
                if (neighbor in Q) and (not neighbor.hk_node):
                    edge = graph.get_edge_between_nodes(node, neighbor)
                    if edge.weight < neighbor.key:
                        # On met a jour le poids et le parent du noeud pour construire le MST
                        neighbor.parent = node
                        neighbor.key = edge.weight
                        # On met a jour le dictionnaire
                        Q[neighbor] = neighbor.key
        # On doit reparcourir les noeuds pour construire l'arbre
        for node in graph.nodes:
            # MODIF: on ajoute le check pour un noeud deconnecte pour compatibilite avec HK
            self.add_node(node)
            if node.parent is not None:
                self.add_edge(graph.get_edge_between_nodes(node, node.parent))
        return

    def primHeap(self, graph, source):
        """
        Construction d'un minimum spanning tree selon l'algorithme de Prim
        :param graph: graphe a partir duquel construire le MST
        :param source: noeud de depart de l'algorithme
        :return: none
        """
        # On initialise un dictionnaire avec priorite
        Q = []
        # On initialise les noeuds
        for node in graph.nodes:
            node.key = float("inf")
            node.parent = None
            # On remplit le dictionnaire avec chaque noeud
            heapq.heappush(Q, node)
        # On initialise la source et on met a jour le dictionnaire
        source.key = 0

        # On itere tant qu'il y a des noeuds dans Q
        while Q:
            heapq.heapify(Q)
            node = heapq.heappop(Q)
            # On recupere les voisins du noeud courant
            for neighbor in graph.get_neighbor_nodes(node):
                # Si un des noeuds voisins n'a pas encore ete visite
                # MODIF: on ajoute le check pour un noeud deconnecte pour compatibilite avec HK
                if (neighbor in Q) and (not neighbor.hk_node):
                    edge = graph.get_edge_between_nodes(node, neighbor)
                    if edge.weight < neighbor.key:
                        # On met a jour le poids et le parent du noeud pour construire le MST
                        neighbor.parent = node
                        neighbor.key = edge.weight
        # On doit reparcourir les noeuds pour construire l'arbre
        for node in graph.nodes:
            # MODIF: on ajoute le check pour un noeud deconnecte pour compatibilite avec HK
            self.add_node(node)
            if node.parent is not None:
                self.add_edge(graph.get_edge_between_nodes(node, node.parent))
        return

    def add_edge(self, edge):
        # On appelle la methode de Graph
        super(MstBuilder, self).add_edge(edge)

        # Mais on remplit un dictionnaire de degres des noeuds

        # On recupere les noeuds de l'arete
        node1, node2 = edge.get_nodes()

        # On check si le dictionnaire existe pour l'entree
        # Si oui on incremente, sinon on initialise
        try:
            self.__node_degree[node1] += 1
        except KeyError:
            self.__node_degree[node1] = 1
        try:
            self.__node_degree[node2] += 1
        except KeyError:
            self.__node_degree[node2] = 1

    def get_node_degree(self, node):
        try:
            return self.__node_degree[node]
        except KeyError:
            return 0

if __name__ == '__main__':

    from node import Node
    from edge import Edge
    from graph import Graph
    import matplotlib.pyplot as plt

    G = Graph(name='Graphe du cours')
    # let = ['a', 'b', 'c', 'd', 'e']
    # dat = [[0, 0], [5, 5], [0, 5], [5, 0], [3, 3]]
    let = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
    dat = [[0, 1], [1, 2], [4, 2], [6, 2], [7, 1], [6, 0], [4, 0], [1, 0], [2, 1]]
    for temp_name, temp_dat in zip(let, dat):
        G.add_node(Node(name=temp_name, data=temp_dat))

    nodes = G.get_nodes()
    #
    # # Building the example from ItA
    # # Should be 14 rows
    nl = [
        [nodes[0], nodes[1], 4],
        [nodes[0], nodes[7], 8],
        [nodes[1], nodes[2], 8],
        [nodes[1], nodes[7], 11],
        [nodes[2], nodes[3], 7],
        [nodes[2], nodes[5], 4],
        [nodes[2], nodes[8], 2],
        [nodes[3], nodes[4], 9],
        [nodes[3], nodes[5], 14],
        [nodes[4], nodes[5], 10],
        [nodes[5], nodes[6], 2],
        [nodes[6], nodes[7], 1],
        [nodes[6], nodes[8], 6],
        [nodes[7], nodes[8], 7],
        ]

    for ind in range(0, len(nl)):
        G.add_edge(Edge(nl[ind][0], nl[ind][1], nl[ind][2]))

    # for i in range(0, len(let)):
    #     for j in range(i + 1, len(let)):
    #         temp_dat1 = nodes[i].get_data()
    #         temp_dat2 = nodes[j].get_data()
    #         dist = ((temp_dat1[0] - temp_dat2[0])**2 + (temp_dat1[1] - temp_dat2[1])**2)**0.5
    #         G.add_edge(Edge(nodes[i], nodes[j], weight=dist))

    G.plot_graph()

    for source in G.nodes:
        mstp = MstBuilder()
        mstp.primHeap(G, source)
        mstp.plot_graph()
        print mstp.get_graph_weight()

    # mstk = MstBuilder(name='MST test Kruskal')
    # mstk.kruskal(G)
    #
    # G.nodes[8].HK_node = True
    #
    # mstk2 = MstBuilder(name='MST test Kruskal 1-tree')
    # mstk2.kruskal(G)
    #
    # mstp2 = MstBuilder(name='MST test Prim 1-tree')
    # mstp2.prim(G, G.nodes[0])
    #
    # G.nodes[8].HK_node = False
    #
    # mstp = MstBuilder(name='MST test Prim')
    # mstp.prim(G, nodes[0])
    #
    # mstk.plot_graph(s='Kruskal')
    # mstk2.plot_graph(s='Kruskal 1-tree')
    # mstp.plot_graph(s='Prim')
    # mstp2.plot_graph(s='Prim 1-tree')
    plt.show()
