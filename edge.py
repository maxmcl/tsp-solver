

class Edge(object):
    """
    Une classe generique pour representer une arete d'un graphe non oriente
    """

    __edge_count = -1   # Compteur global partage par toutes les instances.

    def __init__(self, node1, node2, weight):
        self.__node1 = node1
        self.__node2 = node2
        self.__weight = weight
        Edge.__edge_count += 1
        self.__id = Edge.__edge_count

    def get_weight(self):
        """
        Permet d'obtenir le poids d'une arete
        MODIF: inclue les penalites pour les noeuds avec la methode de HK!
        """
        return self.__weight + self.__node1.pi + self.__node2.pi

    def set_weight(self, weight):
        """
        Permet de modifier le poids d'une arete
        """
        self.__weight = weight

    def get_nodes(self):
        """
        Permet de recuperer les noeuds entre lesquels l'arete se situe
        """
        return self.__node1, self.__node2

    def get_id(self):
        """
        Permet de recuperer l'id de l'arete
        """
        return self.__id

    def get_destination(self):
        return [self.__node1, self.__node2]

    # Proprietes
    weight = property(get_weight, set_weight)

    def __repr__(self):
        """
        Surcharge de la fonction print pour afficher les nouvelles proprietes
        """
        nodes = self.get_nodes()
        s = 'Arete (id %d)' % self.get_id()
        s += ' (Noeuds: (' + str(nodes[0].get_id()) + ', ' + str(nodes[1].get_id()) + ')' + ', poids: ' \
            + str(self.weight) + '),'
        return s
