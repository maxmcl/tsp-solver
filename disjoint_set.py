

class DisjointSet(object):
    """
    Une classe statique pour representer des ensembles disjoint. Ne fait qu'implementer des methodes qui modifient des
    objets de classe Node. Implemente deux heuristiques (union par rang et compression des chemins) afin d'accelerer
    l'algorithme de Kruskal
    """

    def __init__(self):
        """
        Constructeur
        """
        return

    @staticmethod
    def make_set(node):
        """
        Initialise les parametres d'un objet Node pour creer un ensemble disjoint
        :param node: noeud qui deviendra un ensemble disjoint
        :return: none
        """
        node.root = node
        node.rank = 0

    @staticmethod
    def union(node1, node2):
        """
        Connecte les deux ensembles disjoints qui contiennent node1 et node2 en appliquant l'union par rang
        :param node1: noeud dans un ensemble disjoint
        :param node2: noeud dans un autre ensemble disjoint
        :return: none
        """
        DisjointSet.link(DisjointSet.find_set(node1), DisjointSet.find_set(node2))

    @staticmethod
    def link(node1, node2):
        """
        Lie deux ensembles disjoints en appliquant l'union par rang
        :param node1: noeud dans un ensemble disjoint
        :param node2: noeud dans un autre ensemble disjoint
        :return: none
        """
        # Si le noeud 1 est plus haut que le noeud 2
        if node1.rank > node2.rank:
            # La racine du noeud 2 devient le noeud 1
            node2.root = node1
        # Sinon, le noeud 2 est plus haut ou egal au noeud 1
        else:
            # La racine du noeud 1 est le noeud 2
            node1.root = node2
            # Si les deux noeuds etaient d'hauteur egale
            if node1.rank == node2.rank:
                # Le noeud 2 devient un rang plus haut car on met sous lui une chaine de sa longueur
                node2.rank += 1

    @staticmethod
    def find_set(node):
        """
        Trouve la racine d'un ensemble disjoint, implemente la compression des chemins
        :param node: noeud pour lequel on cherche la racine
        :return: noeud representant la racine de l'ensemble disjoint dans lequel node est
        """
        # On veut garder un pointeur sur node pour remonter l'aborescence plus tard
        temp = node
        # On cherche la racine, soit le noeud qui pointe vers lui-meme
        while temp != temp.root:
            # On recupere la racine
            temp = temp.root
        # On a atteint la racine de l'ensemble, on sauvegarde pour retourner ce pointeur
        root = temp

        # On remonte l'arborescence depuis node pour compresser les chemins
        while node != node.root:
            # On sauvegarde un pointeur sur le parent de node
            temp = node.root
            # On compresse le chemin, la racine de node est directement root
            node.root = root
            # On passe au noeud suivant
            node = temp
        return root

