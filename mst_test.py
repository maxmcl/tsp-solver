from graph import Graph
from mst_builder import MstBuilder
from read_stsp import *
import matplotlib.pyplot as plt
# -----------------------------

import sys

filename = sys.argv[1]

print '\n --------------- %s --------------- \n' % filename
# On utilise essentiellement le code fourni dans read_stsp.py
with open(filename, "r") as fd:

    # Lecture du header
    header = read_header(fd)
    dim = header['DIMENSION']
    # Acquisition du format du fichier
    edge_weight_format = header['EDGE_WEIGHT_FORMAT']

    # Lecture des noeuds
    nodes = read_nodes(header, fd)
    # Lecture des arretes
    edges = read_edges(header, fd)

    # On cree un objet de classe Graph
    prob_graph = Graph(name=file)
    # On remplit l'objet Graph avec les noeuds et aretes de l'instance stsp
    prob_graph.graph_from_stsp(nodes, edges)
    # On trace le graph de depart
    # prob_graph.plot_graph(s="Graphe original")

    # mst = MstBuilder()
    # mst.kruskal(prob_graph)
    # opt_tour_weight_kruskal = mst.get_graph_weight()
    # opt_tour_kruskal = mst

    # opt_tour_weight_prim = float('infinity')
    # for ind, source in enumerate(prob_graph.nodes):
    #     # On cree l'instance TSP
    mst = MstBuilder()
    mst.prim(prob_graph, prob_graph.nodes[0])
    #     # On conserve le MST
    #     mst_weight = mst.get_graph_weight()
    #     # print 'source = ' + str(ind) + ', weight = ' + str(mst_weight) + '\n'
    #     if mst_weight < opt_tour_weight_prim:
    #         opt_tour_weight_prim = mst_weight
    #         opt_tour_prim = mst
    #         opt_tour_index_prim = ind

    # On affiche la tournee minimale et son poids
    # print 'Kruskal MST = ' + str(opt_tour_weight_kruskal) + '\n'
    # print 'Prim MST = ' + str(opt_tour_weight_prim) + ', index = ' + str(opt_tour_index_prim) + '\n'

plt.show()
