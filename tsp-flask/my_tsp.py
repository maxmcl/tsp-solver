"""Main TSP hook."""

from tsp_helper import get_distance
from graph import Graph
from node import Node
from edge import Edge
from rsl_solver import RSLSolver
from mst_builder import MstBuilder
import matplotlib.pyplot as plt


def get_visit_order(geoPoints):
    """THIS IS THE ONLY FUNCTION THAT YOU NEED TO MODIFY FOR PHASE 5.

    The only argument, *geoPoints*, is a list of points that user has marked.
    Each element of geoPoints is an instance of the GeoPoint class. You need to
    create your graph using these points. You obtain the distance between two
    points by calling the *getDistance* function; for example:

    get_distance(geoPoints[0], geoPoints[1])

    Run your tsp solver and return the locations visit order. The return value,
    *order*, must be a list of indices of points, specifying the visit order.

    In the example implementation below, we visit each point by the order
    in which they were marked (clicked).
    """
    # nMarks = len(geoPoints)
    # print "fist leg length: ", get_distance(geoPoints[0], geoPoints[1])
    # Building graph
    G = Graph()
    # Building nodes
    for ii, geoPt in enumerate(geoPoints):
        G.add_node(Node(name=repr(ii), data=[geoPt.lat, geoPt.lng]))
    # Building edges for every nodes
    for ii, geoPt1 in enumerate(geoPoints):
        for jj, geoPt2 in enumerate(geoPoints):
            if geoPt1 != geoPt2:
                dist = get_distance(geoPt1, geoPt2)
                G.add_edge(Edge(G.nodes[ii], G.nodes[jj], weight=dist))

    # On itere sur TOUS les noeuds
    opt_tour = None
    opt_tour_weight = float("inf")
    opt_tour_index = None
    for ind, source in enumerate(G.nodes):
        # On cree l'instance TSP
        # On resoud le TSP
        solver = RSLSolver()
        mst_func = MstBuilder.kruskal
        tour_graph = solver.solve(G, source, mst_func)
        # On conserve la tournee minimale
        tour_weight = tour_graph.get_graph_weight()
        # print ind, tour_weight
        if tour_weight <= opt_tour_weight:
            opt_tour_weight = tour_weight
            opt_tour = tour_graph
            opt_tour_index = ind

    # On affiche la tournee minimale et son poids
    print 'Approx tour weight = ' + str(opt_tour_weight) + ', source node = ' + str(opt_tour_index) + '\n'
    opt_tour.plot_graph()
    order = [node.get_id() for node in opt_tour.nodes]  # default order
    order = order + [order[0]]
    return order
