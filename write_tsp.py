"""Write a TSP problem to file in TSPLib format."""

import math


def write_header(fd, header):
    """Write TSPLib header to file descriptor ``fd``.

    ``header`` should be a dict of header key/value pairs.
    """
    if header["TYPE"] != "TSP":
        raise ValueError("only TSP files are supported for now")
    if header["EDGE_WEIGHT_TYPE"] != "EXPLICIT":
        raise ValueError("only explicit edge weights are supported for now")
    if header["EDGE_WEIGHT_FORMAT"] not in ["FULL_MATRIX", "UPPER_ROW"]:
        raise ValueError("only FULL_MATRIX or UPPER_ROW formats are supported")
    if header.get("EDGE_DATA_FORMAT", None) is not None:
        raise ValueError("only complete graphs are supported for now")
    fd.seek(0)
    fd.write("NAME: %s\n" % str(header["NAME"]))
    fd.write("TYPE: %s\n" % str(header["TYPE"]))
    fd.write("COMMENT: %s\n" % str(header.get("COMMENT", "")))
    fd.write("DIMENSION: %d\n" % int(header["DIMENSION"]))
    fd.write("EDGE_WEIGHT_TYPE: %s\n" % str(header["EDGE_WEIGHT_TYPE"]))
    fd.write("EDGE_WEIGHT_FORMAT: %s\n" % str(header["EDGE_WEIGHT_FORMAT"]))
    fd.write("NODE_COORD_TYPE: %s\n" % str(header.get("NODE_COORD_TYPE",
                                                      "NO_COORDS")))
    fd.write("DISPLAY_DATA_TYPE: %s\n" % str(header.get("DISPLAY_DATA_TYPE",
                                                        "NO_DISPLAY")))
    return


def write_node_section(fd, header, nodes):
    """Write node section to TSPLib-formatted file.

    ``header`` should be a dict of header key/value pairs,
    ``nodes`` should be an iterable.
    """
    data = (node.data for node in nodes)
    has_coords = False
    twod = False
    if header["NODE_COORD_TYPE"] in ["TWOD_COORDS", "THREED_COORDS"]:
        has_coords = True
        twod = header["NODE_COORD_TYPE"] == "TWOD_COORDS"
        fd.write("NODE_COORD_SECTION\n")
    elif header.get("DISPLAY_DATA_TYPE", "NO_DISPLAY") == "TWOD_DISPLAY":
        has_coords = True
        twod = True
        fd.write("DISPLAY_DATA_SECTION\n")
    if has_coords:
        nnodes = len(nodes)
        ndigits = math.floor(math.log10(nnodes)) + 1
        kfmt = "{:%dd}" % ndigits
        ncoords = 2 if twod else 3
        coordfmt = "  {:21.15e}" * ncoords
        fmt = kfmt + coordfmt + "\n"
        k = 1
        for datum in data:
            fd.write(fmt.format(k, *datum))
            k = k + 1
    return


def write_edge_section(fd, header, nodes, edges):
    """Write node section to TSPLib-formatted file.

    ``header`` should be a dict of header key/value pairs,
    ``edges`` should be an iterable.
    """
    n = header["DIMENSION"]
    if len(edges) != n:
        return ValueError("len(edges) should equal the number of nodes")
    fd.write("EDGE_WEIGHT_SECTION\n")
    upper_row = header["EDGE_WEIGHT_FORMAT"] == "UPPER_ROW"
    k = 0
    for node in sorted(nodes, key=lambda node: node.get_id()):
        if upper_row:
            sorted_edges = sorted((e for e in edges[node]
                                   if e.get_destination().get_id() > k),
                                  key=lambda e: e.get_destination().get_id())
            n_to_write = n - k - 1
        else:
            sorted_edges = sorted(edges[node],
                                  key=lambda e: e.get_destination().get_id())
            n_to_write = n
        if len(sorted_edges) != n_to_write:
            raise ValueError("number of edges inconsistent with edge format")
        if n_to_write > 0:
            fmt = "{:21.15e} " * (n_to_write - 1) + "{:21.15e}\n"
            fd.write(fmt.format(*(edge.weight for edge in sorted_edges)))
        k = k + 1
    return


def write_tsp(filename, name, nodes, edges, node_coord_type="NO_COORDS"):
    """Write complete graph to file in TSPLib format.

    ``header`` should be a dict of header key/value pairs,
    ``nodes`` should be an iterable
    ``edges`` should be a dict indexed by ``nodes``.
    """
    header = {}
    header["DIMENSION"] = len(nodes)
    header["NAME"] = str(name)
    header["TYPE"] = "TSP"
    header["EDGE_WEIGHT_TYPE"] = "EXPLICIT"
    header["EDGE_WEIGHT_FORMAT"] = "UPPER_ROW"
    header["NODE_COORD_TYPE"] = node_coord_type
    with open(filename + ".tsp", "w") as fd:
        write_header(fd, header)
        write_node_section(fd, header, nodes)
        write_edge_section(fd, header, nodes, edges)
    return
