from graph import Graph
from dfs import dfs
from mst_builder import MstBuilder


class RSLSolver(object):

    def __init__(self):
        pass

    def solve(self, graph, source, mst_func, tol=0, max_counter=0):
        """
        Resoud un probleme de TSP en appliquant l'algorithme de RSL (Approx-TSP-Tour)
        :param graph: instance Graph sur laquelle resoudre le probleme de TSP
        :param source: noeud de depart de l'algorithme dans le graphe
        :param mst_func: on permet de passer la methode de son choix pour creer le MST (Kruskal ou Prim)
        :param tol: parametre non utilise mais doit etre present pour conserver le nombre d'arguments
        :param max_counter: parametre non utilise mais doit etre present pour conserver le nombre d'arguments
        :return: graph object
        """
        # On nettoie le graph si on veut appliquer plusieurs fois la methode
        for node in graph.nodes:
            node.pi = 0

        mst = MstBuilder()
        # Appel de la methode passee sur l'objet MstBuilder
        mst_func(mst, graph, source)

        # On doit faire une depth-first search sur l'arbre (NOTE: on modifie les noeuds)
        dfs(mst, source)

        # L'attribut parent des noeuds indique le parcours a suivre depuis le noeud source
        # On doit maintenant parcourir les noeuds et ajouter les aretes correspondantes
        # On garde un pointeur sur la source
        node = source
        tour_graph = Graph()
        while node.parent is not None:
            tour_graph.add_node(node)
            tour_graph.add_edge(graph.get_edge_between_nodes(node, node.parent))
            node = node.parent
        # On doit fermer la boucle
        tour_graph.add_node(node)
        tour_graph.add_edge(graph.get_edge_between_nodes(node, source))

        return tour_graph

    def solve_provided_mst(self, graph, one_tree, source):
        """
        Resoud un probleme de TSP en appliquant l'algorithme de RSL (Approx-TSP-Tour)
        :param graph: instance Graph sur laquelle resoudre le probleme de TSP
        :param source: noeud de depart de l'algorithme dans le graphe
        :return: graph object
        """
        # On nettoie le graph si on veut appliquer plusieurs fois la methode
        for node in one_tree.nodes:
            node.pi = 0

        # On doit faire une depth-first search sur l'arbre (NOTE: on modifie les noeuds)
        dfs(one_tree, source)

        # L'attribut parent des noeuds indique le parcours a suivre depuis le noeud source
        # On doit maintenant parcourir les noeuds et ajouter les aretes correspondantes
        # On garde un pointeur sur la source
        node = source
        tour_graph = Graph()
        while node.parent is not None:
            tour_graph.add_node(node)
            tour_graph.add_edge(graph.get_edge_between_nodes(node, node.parent))
            node = node.parent
        # On doit fermer la boucle
        tour_graph.add_node(node)
        tour_graph.add_edge(graph.get_edge_between_nodes(node, source))

        return tour_graph

if __name__ == '__main__':

    from node import Node
    from edge import Edge
    from graph import Graph
    from mst_builder import MstBuilder
    import matplotlib.pyplot as plt

    G = Graph(name='Graphe du cours')
    let = ['a', 'b', 'c', 'd', 'e']
    dat = [[0, 0], [5, 5], [0, 5], [5, 0], [3, 3]]
    # let = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
    # dat = [[0, 1], [1, 2], [4, 2], [6, 2], [7, 1], [6, 0], [4, 0], [1, 0], [2, 1]]
    for temp_name, temp_dat in zip(let, dat):
        G.add_node(Node(name=temp_name, data=temp_dat))

    nodes = G.get_nodes()

    for i in range(0, len(let)):
        for j in range(i + 1, len(let)):
            temp_dat1 = nodes[i].get_data()
            temp_dat2 = nodes[j].get_data()
            dist = int(((temp_dat1[0] - temp_dat2[0]) ** 2 + (temp_dat1[1] - temp_dat2[1]) ** 2) ** 0.5)
            G.add_edge(Edge(nodes[i], nodes[j], weight=dist))

    G.plot_graph()
    for source in G.nodes:
        print '\n---\n'
        print source
        tour = RSLSolver()
        tourk = tour.solve(G, source, MstBuilder.kruskal)
        print 'tourk weight = ' + str(tourk.get_graph_weight())

        tour = RSLSolver()
        tourp = tour.solve(G, source, MstBuilder.prim)
        print 'tourp weight = ' + str(tourp.get_graph_weight())

        tourk.plot_graph(s='Tour Kruskal')
        tourp.plot_graph(s='Tour Prim')
    plt.show()
