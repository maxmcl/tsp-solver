"""
Fichier 'main' qui appelle une instance de TSP symetrique.
On utilise en partie le code fourni dans read_stsp.py afin d'acquerir les noeuds et les aretes du probleme.
"""

from graph import Graph
from mst_builder import MstBuilder
from read_stsp import *
import matplotlib.pyplot as plt
from argparse import ArgumentParser
# -----------------------------

# Choix de l'instance TSP et de la methode de MST depuis le prompt
# On a diverses options
parser = ArgumentParser(description='TSP Solver')
parser.add_argument("-s", "--stsp", dest="filename", default='stsp/bays29.tsp', help="Choix de l'instance TSP")
parser.add_argument("-m", "--mstfunc", dest="mst_func", default='prim',
                    help="Choix de la methode de calcul du MST: prim, kruskal")
parser.add_argument("-ts", "--TSPsolver", dest="TSP_solver", default='HK',
                    help="Choix de la methode de resolution de TSP: HK, RSL, NNS")
parser.add_argument("-t", "--timer", dest="timer", default=False, type=bool, help="Activer le profileur: True, False")
parser.add_argument("-tol", "--tolerance", dest="tol", default=10**-3, type=float, help="Tolerance pour le pas dans HK")
parser.add_argument("-mc", "--maxcounter", dest="max_counter", default=5*10**2, type=int,
                    help="Nombre d'iterations maximal pour HK")
parser.add_argument("-sf", "--savefig", dest="check_save", default=False, type=bool,
                    help="Sauvegarde du graph en pdf sous le dossier Figures/")
# Recuperation des valeurs
args = parser.parse_args()

print '\n --------------- %s --------------- \n' % args.filename
# On utilise essentiellement le code fourni dans read_stsp.py
with open(args.filename, "r") as fd:

    # Lecture du header
    header = read_header(fd)
    dim = header['DIMENSION']
    # Acquisition du format du fichier
    edge_weight_format = header['EDGE_WEIGHT_FORMAT']

    # Lecture des noeuds
    nodes = read_nodes(header, fd)
    # Lecture des arretes
    edges = read_edges(header, fd)

    # On cree un objet de classe Graph
    prob_graph = Graph(name=file)
    # On remplit l'objet Graph avec les noeuds et aretes de l'instance stsp
    prob_graph.graph_from_stsp(nodes, edges)
    # On trace le graph de depart
    # prob_graph.plot_graph(s="Graphe original")

    # Choix de l'algorithme de calcul de MST (Kruskal ou Prim (par defaut))
    if args.mst_func == 'kruskal':
        mst_func = MstBuilder.kruskal
        rr = [0]
    else:
        mst_func = MstBuilder.prim
        rr = range(0, prob_graph.get_nb_nodes(), 1)

    # Choix de l'algorithme de resolution de TSP
    if args.TSP_solver == 'RSL':
        from rsl_solver import RSLSolver
        # On appelle RSL
        TSP_solver = 'RSLSolver()'
    else:
        from hk_solver import HKSolver
        # On appelle HK par defaut
        TSP_solver = 'HKSolver()'

    tol = args.tol
    max_counter = args.max_counter

    opt_tour_weight = float('infinity')
    opt_tour_index = None

    # On itere sur TOUS les noeuds
    for ind in rr:
        # On cree l'instance TSP
        tour = eval(TSP_solver)
        # On resoud le TSP
        tour_graph = tour.solve(prob_graph, prob_graph.nodes[ind], mst_func, tol, max_counter)
        # On conserve la tournee minimale
        tour_weight = tour_graph.get_graph_weight()
        # print ind, tour_weight
        if tour_weight <= opt_tour_weight:
            opt_tour_weight = tour_weight
            opt_tour = tour_graph
            opt_tour_index = ind

    # On affiche la tournee minimale et son poids
    print 'Approx tour weight = ' + str(opt_tour_weight) + ', source node = ' + str(opt_tour_index) + '\n'

    # s = args.filename
    # s = s.replace('stsp/', '') + ' - ' + args.TSP_solver + ' - ' + args.mst_func
    # opt_tour.plot_graph(s, args.check_save)

plt.show()
