import math
from mst_builder import MstBuilder
import heapq
from edge import Edge


class HKSolver(object):

    def __init__(self):
        # Inner parameters
        self.vnorm = 1
        self.W = -float("inf")
        self.sum_pi = 0
        self.t = 1
        self.wrong_step = False
        self.counter = 0
        self.last_pi = {}
        self.period = 0
        self.period_counter = 0
        self.first_period = True
        self.opt_pi = {}

    def reset_params(self):
        self.vnorm = 1
        self.W = -float("inf")
        self.sum_pi = 0
        self.t = 1
        self.wrong_step = False
        self.counter = 0
        self.last_pi = {}
        self.period = 0
        self.period_counter = 0
        self.first_period = True
        self.opt_pi = {}

    @staticmethod
    def build_one_tree2(graph, one_tree_source, mst_func):
        one_tree_source.hk_node = True

        # On appelle la methode passee pour construire le MST sur un noeud d'origine qui est dans le graph (evidemment)
        for node in graph.nodes:
            if node != one_tree_source:
                break
        mst_source = node

        one_tree = MstBuilder()
        mst_func(one_tree, graph, mst_source)

        # On cherche les deux aretes de poids minimal depuis le noeud special de 1-arbre
        [edge1, edge2] = heapq.nsmallest(2, graph.get_neighbor_edges(one_tree_source), key=Edge.get_weight)
        one_tree.add_edge(edge1)
        one_tree.add_edge(edge2)
        one_tree_source.hk_node = False

        return one_tree

    @staticmethod
    def build_one_tree(graph, one_tree_source, mst_func):
        # On construit un MST
        one_tree = MstBuilder()
        mst_func(one_tree, graph, one_tree_source)

        far_edge = None
        far_weight = 0
        for node in one_tree.nodes:
            # On cherche une feuille
            if one_tree.get_node_degree(node) == 1:
                # On trouve celle qui a le deuxieme voisin le plus eloigne
                edge = heapq.nsmallest(2, graph.get_neighbor_edges(node), key=Edge.get_weight)[-1]
                if edge.weight > far_weight:
                    far_edge = edge
                    far_weight = far_edge.weight

        one_tree.add_edge(far_edge)
        return one_tree

    def update_step(self, W_tree, one_tree=None):
        self.wrong_step = False
        if W_tree > self.W:
            self.t *= 2
            self.W = W_tree
        elif self.W > W_tree:
            self.t *= 0.5
            self.wrong_step = True
        else:
            self.t *= 0.5

    def update_step2(self, W_tree, one_tree):
        if W_tree >= self.W:
            if self.first_period:
                self.t *= 2
            if self.period_counter > self.period:
                self.period *= 4
            self.W = W_tree
            self.opt_pi = self.last_pi

        if self.period_counter > self.period:
            self.t *= 0.5
            self.period *= 0.5
            self.period_counter = 0
            self.first_period = False
            # Reset to optimal
            for node in one_tree.nodes:
                node.pi = self.opt_pi[node]

    def solve(self, graph, one_tree_source, mst_func, tol=1e-10, max_counter=5e2):

            # Initialisation
            self.reset_params()

            self.period = graph.get_nb_nodes() / 2

            # On nettoie le graph si on veut appliquer plusieurs fois la methode
            for node in graph.nodes:
                node.pi = 0
                self.last_pi[node] = node.pi

            while (self.vnorm != 0) and (self.counter < max_counter) and (self.t > tol) and (self.period > 1):

                one_tree = HKSolver.build_one_tree(graph, one_tree_source, mst_func)

                W_tree = one_tree.get_graph_weight() - 2 * self.sum_pi

                self.update_step2(W_tree, one_tree)

                self.vnorm = 0
                for node in one_tree.nodes:
                    # Si on n'a pas reussi a augmenter la borne
                    if self.wrong_step:
                        node.pi = self.last_pi[node]
                    else:
                        self.last_pi[node] = node.pi
                    v = one_tree.get_node_degree(node) - 2
                    node.pi += self.t * v
                    # On somme les contributions pour evaluer la norme
                    self.vnorm += v ** 2
                    # On somme les contributions des penalites
                    self.sum_pi += node.pi

                self.counter += 1
                self.period_counter += 1
                self.vnorm = math.sqrt(self.vnorm)

            if self.vnorm != 0:
                from rsl_solver import RSLSolver
                TSPtour = RSLSolver()
                tour_graph = TSPtour.solve_provided_mst(graph, one_tree, one_tree_source)
            else:
                for node in one_tree.nodes:
                    node.pi = 0
                tour_graph = one_tree

            return tour_graph

if __name__ == '__main__':

    from node import Node
    from edge import Edge
    from graph import Graph
    from mst_builder import MstBuilder
    import matplotlib.pyplot as plt

    G = Graph(name='Graphe du cours')
    let = ['a', 'b', 'c', 'd', 'e']
    dat = [[0, 0], [5, 5], [0, 5], [5, 0], [3, 3]]
    # let = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
    # dat = [[0, 1], [1, 2], [4, 2], [6, 2], [7, 1], [6, 0], [4, 0], [1, 0], [2, 1]]
    for temp_name, temp_dat in zip(let, dat):
        G.add_node(Node(name=temp_name, data=temp_dat))

    nodes = G.get_nodes()

    for i in range(0, len(let)):
        for j in range(i + 1, len(let)):
            temp_dat1 = nodes[i].get_data()
            temp_dat2 = nodes[j].get_data()
            dist = int(((temp_dat1[0] - temp_dat2[0])**2 + (temp_dat1[1] - temp_dat2[1])**2)**0.5)
            G.add_edge(Edge(nodes[i], nodes[j], weight=dist))

    G.plot_graph()
    for source in G.nodes:
        print '\n---\n'
        print source
        tour = HKSolver()
        tourk = tour.solve(G, source, MstBuilder.kruskal)
        print 'tourk weight = ' + str(tourk.get_graph_weight())

        tour = HKSolver()
        tourp = tour.solve(G, source, MstBuilder.prim)
        print 'tourp weight = ' + str(tourp.get_graph_weight())

        tourk.plot_graph(s='Tour Kruskal')
        tourp.plot_graph(s='Tour Prim')
    plt.show()
