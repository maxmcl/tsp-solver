

class Node(object):
    """
    Une classe generique pour representer les noeuds d'un graphe.
    """

    __node_count = -1   # Compteur global partage par toutes les instances.

    def __init__(self, name='Sans nom', data=None):
        self.__name = name
        self.__data = data
        Node.__node_count += 1
        self.__id = Node.__node_count

        # Attribut pour DFS
        self.__connected = False
        # utilise egalement __parent

        # Attributs pour Kruskal
        self.__rank = 0
        self.__root = self

        # Attributs pour Prim
        self.__parent = None
        self.__key = float("inf")

        # Attribut pour HK
        self.__pi = 0
        self.hk_node = False

    def get_name(self):
        """
        Donne le nom du noeud
        """
        return self.__name

    def get_id(self):
        """
        Donne le numero d'identification du noeud
        """
        return self.__id

    def get_data(self):
        """
        Donne les donnees contenues dans le noeud
        """
        return self.__data

    def set_data(self, data):
        """
        Permet d'inscrire des donnees dans le noeud
        """
        self.__data = data

    def __repr__(self):
        """
        Surcharge de la fonction print
        """
        s = 'Noeud %s (id %d)' % (self.get_name(), self.get_id())
        s += ' (donnees: ' + repr(self.get_data()) + ')'
        return s

    def __eq__(self, node):
        return self.get_id() == node.get_id()

    """
                                        ---- DFS methods ----
    """

    def get_connected(self):
        return self.__connected

    def set_connected(self, connected):
        self.__connected = connected

    """
                                         ---- Kruskal methods ----
    """

    def get_root(self):
        return self.__root

    def set_root(self, new_root):
        self.__root = new_root

    def get_rank(self):
        return self.__rank

    def set_rank(self, new_rank):
        self.__rank = new_rank

    def __lt__(self, other_node):
        return self.key < other_node.key

    def __le__(self, other_node):
        return self.key <= other_node.key

    """
                                        ---- Prim methods ----
    """

    def get_parent(self):
        return self.__parent

    def set_parent(self, parent):
        self.__parent = parent

    def get_key(self):
        return self.__key

    def set_key(self, key):
        self.__key = max(0, key)

    def get_priority(self):
        "Donne la priorite de l'item."
        return self.key

    def set_priority(self, priority):
        "Affecte une priorite a l'item."
        self.key = max(0, priority)

    """
                                            ---- HK methods ----
    """
    def get_pi(self):
        return self.__pi

    def set_pi(self, pi):
        self.__pi = pi

    # Proprietes
    root = property(get_root, set_root)
    rank = property(get_rank, set_rank)
    parent = property(get_parent, set_parent)
    key = property(get_key, set_key)
    connected = property(get_connected, set_connected)
    pi = property(get_pi, set_pi)
    data = property(get_data, set_data)
