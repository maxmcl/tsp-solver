from stack import Stack
from node import Node


def dfs(graph, source):
    """
    Methode qui fait un depth-first search sur un objet Graph
    Node doit posseder les attributs connected et parent
    :param graph: instance graphe sur laquelle effectuer la recherche
    :param source: noeud de depart de l'algorithme
    :return: none
    """
    # On reinitialise les noeuds
    for node in graph.nodes:
        node.parent = None
        node.connected = False

    pile = Stack()
    pile.push(source)
    old_node = Node()
    no_nodes = graph.get_nb_nodes()
    node_counter = 0

    # On itere tant que la pile n'est pas vide
    while (node_counter <= no_nodes) and (not pile.is_empty()):
        node = pile.pop()
        if not node.connected:
            for neighbor in graph.get_neighbor_nodes(node):
                if not neighbor.connected:
                    pile.push(neighbor)
            node.connected = True
            node_counter += 1
            old_node.parent = node
            old_node = node
    return

if __name__ == '__main__':

    from node import Node
    from edge import Edge
    from graph import Graph
    import matplotlib.pyplot as plt

    G = Graph(name='Graphe du cours')
    let = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
    dat = [[0, 1], [1, 2], [4, 2], [6, 2], [7, 1], [6, 0], [4, 0], [1, 0], [2, 1]]
    for temp_name, temp_dat in zip(let, dat):
        G.add_node(Node(name=temp_name, data=temp_dat))

    nodes = G.get_nodes()

    # Building the example from ItA
    # Should be 14 rows
    nl = [
        [nodes[0], nodes[1], 4],
        [nodes[0], nodes[7], 8],
        [nodes[1], nodes[2], 8],
        [nodes[1], nodes[7], 11],
        [nodes[2], nodes[3], 7],
        [nodes[2], nodes[5], 4],
        [nodes[2], nodes[8], 2],
        [nodes[3], nodes[4], 9],
        [nodes[3], nodes[5], 14],
        [nodes[4], nodes[5], 10],
        [nodes[5], nodes[6], 2],
        [nodes[6], nodes[7], 1],
        [nodes[6], nodes[8], 6],
        [nodes[7], nodes[8], 7],
        ]

    for ind in range(0, len(nl)):
        G.add_edge(Edge(nl[ind][0], nl[ind][1], nl[ind][2]))

    order = dfs(G, G.nodes[0])

    temp = G.nodes[0]
    while (temp is not None) and (temp is not temp.parent):
        print temp
        temp = temp.parent
    pass

    G.plot_graph()
    print order
    plt.show()